from src.main import pipe


def test_pipe():
    assert pipe(
        ["infer", "compare", "adj_save"],
        adj_matrix_path="data/adj/Karate Club.csv",
        save_partitions=False,
        print_stats=False,
        save_stats=False,
        show_graphs=False,
        show_mod=False,
    ) is None
