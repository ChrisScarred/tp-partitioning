from src.graph_ops.representation import get_nets
from src.utils.config import ADJ_MATRIX_DIR, ADJ_MATRIX_PATH


def test_csv_reading_with_config() -> None:
    assert get_nets(ADJ_MATRIX_DIR, ADJ_MATRIX_PATH)
