import pytest
import src.utils.custom_typing as ct
from src.graph_ops.clustering import overlay_cluster


@pytest.mark.parametrize(
    "c1, c2, c", [([[0, 1], [2, 3, 4]], [[0, 3], [1, 2, 4]], [[0], [1], [2, 4], [3]])]
)
def test_overlay_cluster(c1: ct.Partition, c2: ct.Partition, c: ct.Partition):
    assert overlay_cluster(c1, c2) == c
