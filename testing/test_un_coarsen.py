from typing import Any, Dict, List

import pytest
import src.utils.custom_typing as ct
from src.partitioners.label_prop import LabelProp
from testing.data import (
    KARATE_COARSE_PARTITIONS,
    KARATE_OG_PARTITIONS,
    KARATE_SCHEMES,
)

LP = LabelProp()


@pytest.mark.parametrize(
    "scheme, partition",
    [
        (scheme, partition)
        for scheme, partition in zip(KARATE_SCHEMES, KARATE_COARSE_PARTITIONS)
    ],
)
def test_coarse_partition_to_scheme(scheme: Dict, partition: ct.Partition):
    assert scheme == LP.coarse_partition_to_scheme(partition)


@pytest.mark.parametrize(
    "schemes, og_partition",
    [
        (KARATE_SCHEMES[: i + 1], partition)
        for i, partition in enumerate(KARATE_OG_PARTITIONS)
    ],
)
def test_uncoarsen(
    schemes: List[Dict[Any, int]],
    og_partition: ct.Partition,
):
    assert LP.uncoarsen(schemes) == og_partition
