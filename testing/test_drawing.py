from src.draw import draw, plot_fitness
from src.graph_ops.generator import get_karate_club

GRAPH, COMMS = get_karate_club()


def test_drawing():
    assert draw(GRAPH) is None

def test_drawing_with_communities():
    assert draw(GRAPH, communities=COMMS) is None

def test_plot_fitness():
    assert plot_fitness([0, 1, 2, 2, 3, 4, 4]) is None
