import networkx as nx
from src.partitioners.louvain import Louvain
from src.partitioners.memevo import MemEvo
from src.partitioners.label_prop import LabelProp
from src.partitioners.laplacian_method import LaplacianMethod
from src.partitioners.spectral_clustering import SpectralClustering

GRAPH = nx.karate_club_graph()


def test_louvain():
    assert nx.algorithms.community.community_utils.is_partition(
        GRAPH, Louvain(graph=GRAPH).partition()[0]
    )


def test_memevo():
    assert nx.algorithms.community.community_utils.is_partition(
        GRAPH, MemEvo(graph=GRAPH).partition()[0]
    )


def test_label_prop():
    assert nx.algorithms.community.community_utils.is_partition(
        GRAPH, LabelProp(graph=GRAPH).partition()
    )

def test_laplacian_method():
    assert nx.algorithms.community.community_utils.is_partition(
        GRAPH, LaplacianMethod(graph=GRAPH).partition()[0]
    )

def test_spectral_clustering():
    assert nx.algorithms.community.community_utils.is_partition(
        GRAPH, SpectralClustering(graph=GRAPH).partition()[0]
    )
