# Transcriptome-Proteome Partitioning

NWI-IMC042 Natural Computing 2023/2024 project of group 26 (Tousif Jamal, Giorgio Nagy, Chris Vajdík).

## Instructions

1. Obtain required dependencies specified in `requirements.txt` or `pyproject.toml`.
2. Copy `example.env` to `.env` and fill out the values depending on your local config.
3. Run the main pipeline through `python -m src.main` from the project's root directory.
4. Run (unit) tests through `python -m pytest` from the project's root directory.
