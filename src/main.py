from typing import List, Optional

from src.graph_ops.generator import GraphGenerator

from src.inference import infer
from src.utils.config import (
    ACTIONS,
    ADJ_MATRIX_DIR,
    ADJ_MATRIX_PATH,
    DIR_ADJ,
    DIR_PARTITIONS,
    DIR_STATS,
    SAVE_PARTITIONS,
    SAVE_STATS,
    SHOW_GRAPHS,
    SHOW_MOD,
    SHOW_STATS,
    SAVE_GRAPHS,
    SAVE_MOD,
    DIR_FIGS,
)


def pipe(
    actions: List[str],
    adj_dir: str = DIR_ADJ,
    adj_matrix_dir: Optional[str] = ADJ_MATRIX_DIR,
    adj_matrix_path: Optional[str] = ADJ_MATRIX_PATH,
    save_partitions: bool = SAVE_PARTITIONS,
    part_dir: str = DIR_PARTITIONS,
    print_stats: bool = SHOW_STATS,
    save_stats: bool = SAVE_STATS,
    stats_dir: bool = DIR_STATS,
    show_graphs: bool = SHOW_GRAPHS,
    show_mod: bool = SHOW_MOD,
    save_graphs: bool = SAVE_GRAPHS,
    save_mod: bool = SAVE_MOD,
    dir_figs: str = DIR_FIGS,
) -> None:
    if "adj_save" in actions:
        gg = GraphGenerator()
        gg.get_synth_nets(save_matrices=True, dir_adj=adj_dir)
    if "compare" in actions or "infer" in actions:
        inference = lambda x: infer(
            adj_matrix_dir,
            adj_matrix_path,
            save_partitions,
            part_dir,
            print_stats,
            save_stats,
            stats_dir,
            show_graphs,
            show_mod,
            save_graphs,
            save_mod,
            dir_figs,
            x,
        )

        if "infer" in actions:
            inference(False)
        if "compare" in actions:
            inference(True)


if __name__ == "__main__":
    pipe(ACTIONS)
