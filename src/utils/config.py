import math
import os

import networkx as nx
from dotenv import load_dotenv

load_dotenv()

# subset of "infer", "compare", "adj_save"
ACTIONS = ["compare"]

# either path or directory should be supplied, not both
# Specify the path or directory in the file .env in the root of the project
ADJ_MATRIX_PATH = os.getenv("ADJ_MATRIX_PATH")
ADJ_MATRIX_DIR = os.getenv("ADJ_MATRIX_DIR")

# path to the directories for saving adjacency matrices, partitions, and stats
DIR_ADJ = "data/adj"
DIR_PARTITIONS = "data/parts"
DIR_STATS = "data/stats"
DIR_FIGS = "data/figs"

# defaults for saving partitions and stats
SAVE_PARTITIONS = True
SAVE_STATS = True
SAVE_GRAPHS = True
SAVE_MOD = True
SAVE_BEESWARM = True
SAVE_AGG = True

# defaults for showing stats and graphs
SHOW_STATS = False
SHOW_GRAPHS = False
SHOW_MOD = False
SHOW_BEESWARM = False
SHOW_AGG = True

# how many times to run each algorithm on each network during comparison
REPEAT = 2

# fitness evaluation function to use for all algorithms
FIT = lambda graph, x: nx.community.modularity(graph, x)

# MemEvo config
MEMEVO_N = 3  # the size of the population
MEMEVO_K = 2  # the size of tournaments
MEMEVO_MP = 0.3  # mutation probability
MEMEVO_PN = 2  # number of specimens required for procreation
MEMEVO_MAX_ITERS = 500
MEMEVO_U_LOW = lambda x: x / 10  # minimum for the maximum allowed community size
MEMEVO_U_HIGH = lambda x: x  # maximum for the maximum allowed community size
MEMEVO_LAMBDA_LOW = 0  # minimum number of label prop iterations
MEMEVO_LAMBDA_HIGH = 4  # maximum number of label prop iterations
MEMEVO_LP_TH = 0.05  # label prop early stopping threshold
MEMEVO_NC_ITERS = 10  # maximum allowed number of no-change MemEvo iterations
MEMEVO_C_TH = (
    0.001  # maximum fitness difference for two partitions to be considered no-change
)
MEMEVO_MU_LOW = lambda x: math.ceil(
    len(x) / 100
)  # minimum number of partitions to mutate in a mutation event
MEMEVO_MU_HIGH = lambda x: math.ceil(
    len(x) / 6
)  # maximum number of partitions to mutate in a mutation event

# default seed
SEED = 10

# drawing and plotting config
COLOUR_LIST = [
    "red",
    "blue",
    "forestgreen",
    "gold",
    "darkorange",
    "deeppink",
    "dodgerblue",
    "lawngreen",
]
RELABEL = False
NODE_COLOUR = "cornflowerblue"
FONT_FAMILY = "sans-serif"
EDGE_COLOUR = "k"
EDGE_ALPHA = 0.5
AX_MARGIN = 0.08
DRAW_LABELS = False
WEIGHT_ATTR = "weight"
FIT_XLABEL = "# Communities"
FIT_YLABEL = "Modularity"
FIT_TITLE = "Modularity"
FIT_LINESTYLE = "-"
FIT_MARKER = "o"

# GraphGenerator config
N_RAND_MIN = 50
N_RAND_MAX = 500
P_RAND = 0.15
W_RAND_MIN = 1
W_RAND_MAX = 25

N_CLIQ_MIN = 10
N_CLIQ_MAX = 50
SIZE_CLIQ_MIN = 5
SIZE_CLIQ_MAX = 50

N_DIV_MIN = 50
N_DIV_MAX = 500
P_DIV_MIN = 10
P_DIV_MAX = 30

COMM_N = [5]
COMM_SIZE = [8]
COMM_P = [0.75]
CLIQ_N = [5]
CLIQ_SIZE = [8]
DIV_N = [100, 200]
DIV_P = [0.25, 0.1]

# LabelProp settings
LP_MAX_ITERS = 10

# SpectralClustering and LaplacianMethod
MAX_COMMUNITIES = 10
