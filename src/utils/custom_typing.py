from typing import Any, Callable, Dict, List, Optional, Tuple, Union

import networkx as nx

Node = int
Cluster = int

Fitness = float
Partition = List[List[Node]]
PartitionDict = Dict[Node, Cluster]
Population = List[Partition]
WeightedEdge = Tuple[Any, Any, int]

Layout = Dict[Any, Union[List[float], Tuple[float]]]
Colour = Union[float, str, Tuple[int, int, int], Tuple[int, int, int, float]]

AlgName = str
GraphName = str
Memory = int
Time = float
GroundTruth = Optional[Partition]
AlgorithmInfo = Tuple[AlgName, Callable]
GraphInfo = Tuple[GraphName, Tuple[nx.Graph, GroundTruth]]
