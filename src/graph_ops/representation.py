import json
import os
import time
from typing import Dict, List, Optional

import networkx as nx
import numpy as np
import pandas as pd
import src.utils.custom_typing as ct
from src.draw import beeswarm_plot, draw, plot_fitness
from src.graph_ops.generator import GraphGenerator
from src.graph_ops.rand_index import calculate_rand_index

from tabulate import tabulate


def partition_as_dict(partition: ct.Partition) -> Dict:
    dict_partition = {}
    for i, p in enumerate(partition):
        for n in p:
            dict_partition[n] = i
    return dict_partition


def partition_from_dict(dct: ct.PartitionDict) -> ct.Partition:
    communities = list(set(dct.values()))
    partition = []
    for com in communities:
        partition.append([k for k, v in dct.items() if v == com])
    return [set(p) for p in partition]


def nx_from_csv(path: str) -> nx.Graph:
    try:
        # for header and index column files
        return nx.from_pandas_adjacency(pd.read_csv(path, index_col=0))
    except nx.exception.NetworkXError:
        # for matrix-only files
        return nx.from_pandas_adjacency(pd.read_csv(path, index_col=None, header=None))


def get_nets(adj_dir: Optional[str], adj_path=Optional[str]) -> List[ct.GraphInfo]:
    if adj_dir:
        adj_dir = os.path.abspath(adj_dir)
        return [
            (os.path.basename(f), (nx_from_csv(os.path.join(adj_dir, f)), None))
            for f in os.listdir(adj_dir)
            if os.path.isfile(os.path.join(adj_dir, f))
            and os.path.splitext(f)[-1].lower() == ".csv"
        ]
    adj_path = os.path.abspath(adj_path)
    return [(os.path.basename(adj_path), (nx_from_csv(adj_path), None))]


def extend_nets(nets: List[ct.GraphInfo]) -> List[ct.GraphInfo]:
    gg = GraphGenerator()
    nets.extend(gg.get_synth_nets())
    return nets


def process_results(
    data: pd.DataFrame,
    save_parts: bool,
    part_dir: str,
    print_stats: bool,
    save_stats: bool,
    stats_dir: bool,
    show_graphs: bool,
    algorithm_comparison: bool,
    show_mod: bool,
    save_graphs: bool,
    save_mod: bool,
    dir_figs: bool,
) -> pd.DataFrame:
    def process(data: pd.DataFrame) -> pd.DataFrame:
        getter = lambda x: data[x].to_numpy()[0]
        graph = getter("network")
        graph_name = getter("network_name")
        ground_truth = getter("ground_truth")
        if print_stats:
            print(graph_name)
        if ground_truth:
            if show_graphs or save_graphs:
                if show_graphs:
                    print("Ground truth communities")
                draw(
                    graph,
                    communities=ground_truth,
                    show_graphs=show_graphs,
                    save_graphs=save_graphs,
                    fig_dir=dir_figs,
                    title=f"{graph_name}_ground_truth",
                )
        for alg_data in data.itertuples(index=False):
            alg_name = alg_data.algorithm_name
            partition = alg_data.partition
            if show_graphs or save_graphs:
                try:
                    if show_graphs:
                        print(alg_name)
                    draw(
                        graph,
                        communities=partition,
                        show_graphs=show_graphs,
                        save_graphs=save_graphs,
                        fig_dir=dir_figs,
                        title=f"{graph_name}_{alg_name}",
                    )
                except nx.exception.NetworkXError:
                    print(
                        f"Cannot draw network {graph_name} partitioned by algorithm {alg_name}"
                    )
            if save_parts:
                with open(
                    os.path.join(
                        part_dir, f"{graph_name}_{alg_name}_{time.time()}.json"
                    ),
                    "w",
                ) as f:
                    partition = [list(p) for p in partition]
                    json.dump(partition, f)
            if show_mod or save_mod:
                label = (
                    "# Communities"
                    if alg_name in ["Spectral Clustering", "Laplacian Method"]
                    else "Iteration"
                )
                plot_fitness(
                    alg_data.fitness,
                    xlabel=label,
                    title=f"Modularity over {label} on {graph_name} Partitioned by {alg_name}",
                    show_graphs=show_mod,
                    save_graphs=save_mod,
                    fig_dir=dir_figs,
                )

        data["modularity"] = data.apply(lambda x: max(x["fitness"]), axis=1)
        data["rand_index"] = data.apply(
            lambda row: calculate_rand_index(row["partition"], row["ground_truth"]),
            axis=1,
        )
        data["communities"] = data.apply(lambda x: len(x["partition"]), axis=1)
        data = data.drop(columns=["network", "ground_truth", "fitness", "partition"])

        data = data[
            [
                "network_name",
                "algorithm_name",
                "communities",
                "modularity",
                "rand_index",
                "exec_time",
                "memory",
                "peak",
            ]
        ]

        data = data.reset_index()
        data = data.drop(columns={"index"})

        if print_stats:
            print(tabulate(data, headers="keys", tablefmt="psql"))

        if save_stats:
            fname = f"{graph_name}_inference_stats_{time.time()}.csv"
            if algorithm_comparison:
                fname = f"{graph_name}_comparison_stats_{time.time()}.csv"
            data.to_csv(os.path.join(stats_dir, fname))

        return data

    if algorithm_comparison:
        final_data = pd.DataFrame()
        for _, group_data in data.groupby("network_name"):
            new_data = process(group_data)
            final_data = pd.concat([final_data, new_data], ignore_index=True)
        return final_data

    return process(data)


def get_graphs(
    adj_dir: Optional[str],
    adj_path: Optional[str],
    algorithm_comparison: bool,
) -> List[ct.GraphInfo]:
    graphs = get_nets(adj_dir, adj_path)
    if algorithm_comparison:
        graphs = extend_nets(graphs)
    return graphs


def aggregate_data(
    data: pd.DataFrame, n_runs: int, show_stats: bool, save_stats: bool, stats_dir: str
) -> None:
    def groupping(
        condition: str, groupped_data: pd.DataFrame, dct: Dict
    ) -> pd.DataFrame:
        cols = []
        values = []
        string = f"{condition}:\n"
        cols.append("condition")
        values.append(condition)
        for key in [
            "communities",
            "modularity",
            "rand_index",
            "exec_time",
            "memory",
            "peak",
        ]:
            vals = groupped_data[key].to_list()
            vals = [v for v in vals if isinstance(v, float) or isinstance(v, int)]
            if len(vals) > 0:
                if not dct.get(key):
                    dct[key] = {}
                dct[key][condition] = vals
                avg = np.mean(vals) if len(vals) > 1 else vals[0]
                med = np.median(vals) if len(vals) > 1 else vals[0]
                sd = np.std(vals) if len(vals) > 1 else 0
                cols.append(f"{key}_avg")
                values.append(avg)
                cols.append(f"{key}_median")
                values.append(med)
                cols.append(f"{key}_sd")
                values.append(sd)
                string += (
                    f"\t Average {key} = {avg:.2f} +- {sd:.2f} and median {med:.2f}\n"
                )
        return pd.DataFrame(data=[values], columns=cols), dct, string

    groupped_alg = pd.DataFrame()
    groupped_alg_net = pd.DataFrame()
    dct_alg = {}
    dct_alg_net = {}
    string_info_alg = ""
    string_info_alg_net = ""
    for alg_name, group_data in data.groupby("algorithm_name"):
        df, dct_alg, string = groupping(alg_name, group_data, dct_alg)
        string_info_alg += string
        groupped_alg = pd.concat([groupped_alg, df], ignore_index=True)
        for net_name, subgroup_data in group_data.groupby("network_name"):
            df, dct_alg_net, string = groupping(
                f"{alg_name} - {net_name}", subgroup_data, dct_alg_net
            )
            groupped_alg_net = pd.concat([groupped_alg_net, df], ignore_index=True)
            string_info_alg_net += string

    if show_stats:
        print(string_info_alg)
        print("\n\n- - -\n\n")
        print(string_info_alg_net)

    if save_stats:
        path = os.path.abspath(stats_dir)
        groupped_alg.to_csv(
            os.path.join(path, f"comparison_groupped_by_algorithm_{time.time()}.csv")
        )
        groupped_alg_net.to_csv(
            os.path.join(
                path, f"comparison_groupped_by_algorithm_network_{time.time()}.csv"
            )
        )

    for key, vals in dct_alg.items():
        beeswarm_plot(
            vals,
            title=f"{key.capitalize()} Benchmark on {n_runs} Runs",
            ylabel=key.capitalize(),
        )

    nets = set(data["network_name"].to_list())
    net_conds = {}
    for net in nets:
        net_conds[net] = {}
        for key, dct in dct_alg_net.items():
            net_conds[net][key] = {}
            for condition, vals in dct.items():
                alg, nt = condition.split(" - ")
                if nt == net:
                    net_conds[net][key][alg] = vals

    for net, dct in net_conds.items():
        for key, vals in dct.items():
            beeswarm_plot(
                vals,
                title=f"{key.capitalize()} Benchmark on {n_runs} Runs on Network {net}",
                ylabel=key.capitalize(),
            )
