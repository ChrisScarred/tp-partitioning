from typing import List, Optional

import src.utils.custom_typing as ct
from sklearn.metrics.cluster import adjusted_rand_score


def convert_clusters_to_membership_list(clusters: ct.Partition) -> List[ct.Node]:
    """Source for this function:
    https://igraph.discourse.group/t/convert-2d-python-list-of-communities-to-vertexcluster/106
    """

    flat_cluster_list = [x for xs in clusters for x in xs]
    number_of_nodes = len(flat_cluster_list)
    membership_list = [None] * number_of_nodes

    for cluster_index, cluster in enumerate(clusters):
        for node in cluster:
            membership_list[node] = cluster_index

    return membership_list


def calculate_rand_index(
    partition: ct.Partition, ground_truth_communities: ct.Partition
) -> Optional[float]:
    """Source for this function:
    https://scikit-learn.org/stable/modules/generated/sklearn.metrics.rand_score.html
    """
    if ground_truth_communities:
        labels_true = convert_clusters_to_membership_list(partition)
        labels_pred = convert_clusters_to_membership_list(ground_truth_communities)
        rand_index = adjusted_rand_score(labels_true, labels_pred)
        return rand_index
    else:
        return None
