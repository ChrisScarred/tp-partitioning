import src.utils.custom_typing as ct

from src.graph_ops.representation import partition_as_dict


def overlay_cluster(c1: ct.Partition, c2: ct.Partition) -> ct.Partition:
    to_set = lambda x: [set(i) for i in x]
    c1 = to_set(c1)
    c2 = to_set(c2)
    nodes = sorted(list(set([v for s in c1 for v in s])))
    c1 = partition_as_dict(c1)
    c2 = partition_as_dict(c2)
    c = []
    exc = []
    for i, node_a in enumerate(nodes):
        if not node_a in exc:
            part = [node_a]
            for j in range(i + 1, len(nodes)):
                node_b = nodes[j]
                if node_b not in exc:
                    if c1[node_a] == c1[node_b] and c2[node_a] == c2[node_b]:
                        part.append(node_b)
                        exc.append(node_b)
            c.append(part)
            exc.append(node_a)
    return c
