import os
import random
from itertools import product
from typing import Any, Iterator, List, Optional, Tuple, Union

import networkx as nx
import numpy as np
from src.utils.config import (
    CLIQ_N,
    CLIQ_SIZE,
    COMM_N,
    COMM_P,
    COMM_SIZE,
    DIR_ADJ,
    DIV_N,
    DIV_P,
    N_CLIQ_MAX,
    N_CLIQ_MIN,
    N_DIV_MAX,
    N_DIV_MIN,
    N_RAND_MAX,
    N_RAND_MIN,
    P_DIV_MAX,
    P_DIV_MIN,
    P_RAND,
    SEED,
    SIZE_CLIQ_MAX,
    SIZE_CLIQ_MIN,
    W_RAND_MAX,
    W_RAND_MIN,
)


def get_safe_multi(a: List[Any], b: List[Any]) -> Iterator[Any]:
    for i, j in zip(a, b):
        yield i or j


class GraphGenerator:
    def __init__(
        self,
        n_random_min: int = N_RAND_MIN,
        n_random_max: int = N_RAND_MAX,
        p_random: float = P_RAND,
        seed: int = SEED,
        w_min_random: int = W_RAND_MIN,
        w_max_random: int = W_RAND_MAX,
        n_cliques_min: int = N_CLIQ_MIN,
        n_cliques_max: int = N_CLIQ_MAX,
        size_cliques_min: int = SIZE_CLIQ_MIN,
        size_cliques_max: int = SIZE_CLIQ_MAX,
        n_div_min: int = N_DIV_MIN,
        n_div_max: int = N_DIV_MAX,
        p_div_min: int = P_DIV_MIN,
        p_div_max: int = P_DIV_MAX,
        comm_n: List[int] = COMM_N,
        comm_size: List[int] = COMM_SIZE,
        comm_p: List[float] = COMM_P,
        cliq_n: List[int] = CLIQ_N,
        cliq_size: List[int] = CLIQ_SIZE,
        div_n: List[int] = DIV_N,
        div_p: List[float] = DIV_P,
    ) -> None:
        self.n_random_min = n_random_min
        self.n_random_max = n_random_max
        self.p_random = p_random
        self.r = random.Random(seed)
        self.w_min_random = w_min_random
        self.w_max_random = w_max_random
        self.n_cliques_min = n_cliques_min
        self.n_cliques_max = n_cliques_max
        self.size_cliques_min = size_cliques_min
        self.size_cliques_max = size_cliques_max
        self.n_div_min = n_div_min
        self.n_div_max = n_div_max
        self.p_div_min = p_div_min
        self.p_div_max = p_div_max
        self.comm_n = comm_n
        self.comm_size = comm_size
        self.comm_p = comm_p
        self.cliq_n = cliq_n
        self.cliq_size = cliq_size
        self.div_n = div_n
        self.div_p = div_p

    def random_graph(
        self,
        n_min: Optional[int] = None,
        n_max: Optional[int] = None,
        weighted: bool = False,
        w_min: Optional[int] = None,
        w_max: Optional[int] = None,
        r: Optional[random.Random] = None,
        p: Optional[float] = None,
    ) -> nx.Graph:
        r = self._set_r(r)
        n_min, n_max, w_min, w_max, p = get_safe_multi(
            [n_min, n_max, w_min, w_max, p],
            [
                self.n_random_min,
                self.n_random_max,
                self.n_random_min,
                self.n_random_min,
                self.p_random,
            ],
        )

        n = r.randrange(n_min, n_max)
        adj = np.zeros((n, n))
        for i, j in product(range(n), range(n)):
            if r.random() < p:
                adj[i, j] = 1
                adj[j, i] = 1
        graph = nx.Graph(adj)
        if not weighted:
            return graph
        for i, j in graph.edges():
            graph.add_edge(i, j, weight=r.randrange(w_min, w_max + 1))
        return graph

    def ring_of_cliques(
        self,
        n_min: Optional[int] = None,
        n_max: Optional[int] = None,
        size_min: Optional[int] = None,
        size_max: Optional[int] = None,
        r: Optional[random.Random] = None,
    ) -> nx.Graph:
        r = self._set_r(r)
        n_min, n_max, size_min, size_max = get_safe_multi(
            [n_min, n_max, size_min, size_max],
            [
                self.n_cliques_min,
                self.n_cliques_max,
                self.size_cliques_min,
                self.size_cliques_max,
            ],
        )

        return nx.ring_of_cliques(
            num_cliques=r.randrange(n_min, n_max + 1),
            clique_size=r.randrange(size_min, size_max + 1),
        )

    def _set_r(self, r: Optional[Union[random.Random, int]] = None) -> random.Random:
        if not isinstance(r, random.Random):
            if isinstance(r, int):
                return random.Random(r)
            return self.r
        return r

    def duplication_divergence(
        self,
        n_min: Optional[int] = None,
        n_max: Optional[int] = None,
        p_min: Optional[int] = None,
        p_max: Optional[int] = None,
        r: Optional[random.Random] = None,
    ) -> nx.Graph:
        r = self._set_r(r)
        n_min, n_max, p_min, p_max = get_safe_multi(
            [n_min, n_max, p_min, p_max],
            [self.n_div_min, self.n_div_max, self.p_div_min, self.p_div_max],
        )

        return nx.duplication_divergence_graph(
            n=r.randrange(n_min, n_max + 1),
            p=r.randrange(p_min, p_max + 1) / 100,
        )

    def random_communities(
        self,
        n_cliques: Optional[int] = None,
        clique_size: Optional[int] = None,
        p_change: Optional[float] = None,
        r: Optional[random.Random] = None,
    ) -> nx.Graph:
        r = self._set_r(r)
        n_cliques, clique_size, p_change = get_safe_multi(
            [n_cliques, clique_size, p_change],
            [self.comm_n[0], self.comm_size[0], self.comm_p[0]],
        )

        graph = nx.ring_of_cliques(n_cliques, clique_size)

        n_e = graph.number_of_edges()
        k = min(n_e, round(n_e * p_change))
        graph.remove_edges_from(r.sample(list(graph.edges()), k))

        candidates = [
            (i, j)
            for i in range(graph.number_of_nodes())
            for j in range(i)
            if not graph.has_edge(i, j)
        ]
        graph.add_edges_from(r.sample(candidates, k))

        return graph

    def get_synth_nets(
        self,
        get_karate: bool = True,
        comm_n: Optional[List[int]] = None,
        comm_size: Optional[List[int]] = None,
        comm_p: Optional[List[float]] = None,
        cliq_n: Optional[List[int]] = None,
        cliq_size: Optional[List[int]] = None,
        div_n: Optional[List[int]] = None,
        div_p: Optional[List[float]] = None,
        r: Optional[random.Random] = None,
        save_matrices: bool = False,
        dir_adj: str = DIR_ADJ,
    ) -> List[Tuple[str, Tuple[nx.Graph, Optional[List[List[Any]]]]]]:
        """Obtain a set of synthetic networks with specified characteristics.

        Types of graph are:
            Random Communities ([x], [y], [z]): a random graph with [x] known communities of size [y] created
            by taking a ring of cliques graph, randomly removing [z]% of its edges, and adding
            the same number of random edges.
            Karate Club: Social network weighted graph of Zachary's Karate Club with 2 communities
            Ring of cliques ([x], [y]): a graph of [x] complete subgraphs of [y] nodes connected
            through single edge
            Duplication Divergence ([x], [y]): A graph of [x] nodes created by duplicating
            the initial nodes and retaining edges incident to the original nodes with a retention
            probability [y]

        Returns:
            List[Tuple[str, Tuple[nx.Graph, Optional[List[List[Any]]]]]]: name of the network
            followed by the tuple of the graph and its communities if they are known.
        """
        r = self._set_r(r)
        comm_n, comm_size, comm_p, cliq_n, cliq_size, div_n, div_p = get_safe_multi(
            [comm_n, comm_size, comm_p, cliq_n, cliq_size, div_n, div_p],
            [
                self.comm_n,
                self.comm_size,
                self.comm_p,
                self.cliq_n,
                self.cliq_size,
                self.div_n,
                self.div_p,
            ],
        )

        graphs = []

        if get_karate:
            graphs.append(("Karate Club", get_karate_club()))

        for n, s, p in zip(comm_n, comm_size, comm_p):
            graphs.append(
                (
                    f"Random Communities ({n}, {s}, {p})",
                    (self.random_communities(n, s, p, r), get_clique_comms(n, s)),
                )
            )

        for n, s in zip(cliq_n, cliq_size):
            graphs.append(
                (
                    f"Ring of Cliques ({n}, {s})",
                    (nx.ring_of_cliques(n, s), get_clique_comms(n, s)),
                )
            )

        for n, p in zip(div_n, div_p):
            graphs.append(
                (
                    f"Duplication Divergence ({n}, {p})",
                    (nx.duplication_divergence_graph(n=n, p=p, seed=r), None),
                )
            )

        if save_matrices:
            for name, (graph, _) in graphs:
                np.savetxt(
                    os.path.join(dir_adj, f"{name}.csv"),
                    nx.to_numpy_array(graph),
                    delimiter=",",
                    fmt="%d",
                )

        return graphs


def get_karate_club(keyword: str = "club") -> Tuple[nx.Graph, List[List[int]]]:
    graph = nx.karate_club_graph()
    n = graph.number_of_nodes()
    comms = []
    comm_names = set([graph.nodes[n][keyword] for n in range(n)])
    for name in comm_names:
        comms.append([n for n in range(n) if graph.nodes[n][keyword] == name])
    return graph, comms


def get_clique_comms(n_cliques: int, clique_size: int) -> List[List[int]]:
    return [
        list(np.arange(i * clique_size, (i + 1) * clique_size))
        for i in range(n_cliques)
    ]
