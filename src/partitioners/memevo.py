import random
from itertools import product
from typing import Any, Callable, List, Optional, Tuple

import networkx as nx
import numpy as np
import src.utils.custom_typing as ct
from src.graph_ops.clustering import overlay_cluster
from src.partitioners.label_prop import LabelProp
from src.partitioners.louvain import Louvain
from src.partitioners.models import GeneticAlgorithm, get_fittest, Partitioner
from src.utils.config import (
    FIT,
    MEMEVO_C_TH,
    MEMEVO_K,
    MEMEVO_LAMBDA_HIGH,
    MEMEVO_LAMBDA_LOW,
    MEMEVO_LP_TH,
    MEMEVO_MAX_ITERS,
    MEMEVO_MP,
    MEMEVO_MU_HIGH,
    MEMEVO_MU_LOW,
    MEMEVO_N,
    MEMEVO_NC_ITERS,
    MEMEVO_PN,
    MEMEVO_U_HIGH,
    MEMEVO_U_LOW,
    SEED,
)


class MemEvo(Partitioner, GeneticAlgorithm):
    def __init__(
        self,
        graph: Optional[nx.Graph] = None,
        n: int = MEMEVO_N,
        k: int = MEMEVO_K,
        mp: float = MEMEVO_MP,
        pn: int = MEMEVO_PN,
        fit: Callable = FIT,
        max_iters: int = MEMEVO_MAX_ITERS,
        u_low: Callable = MEMEVO_U_LOW,
        u_high: Callable = MEMEVO_U_HIGH,
        lambda_low: int = MEMEVO_LAMBDA_LOW,
        lambda_high: int = MEMEVO_LAMBDA_HIGH,
        th: float = MEMEVO_LP_TH,
        nc_iters: int = MEMEVO_NC_ITERS,
        c_th: int = MEMEVO_C_TH,
        mu_low: Callable = MEMEVO_MU_LOW,
        mu_high: Callable = MEMEVO_MU_HIGH,
        seed: int = SEED,
    ) -> None:
        self._graph = self._graph_preprocess(graph)
        self.louvain = Louvain(graph)
        self.label_prop = LabelProp(graph)
        self.population = []
        self.n = n
        self.k = k
        self.mp = mp
        self.pn = pn
        self.fitness = fit
        self.max_iters = max_iters
        self.u_low = u_low
        self.u_high = u_high
        self.lambda_low = lambda_low
        self.lambda_high = lambda_high
        self.th = th
        self.nc_iters = nc_iters
        self.c_th = c_th
        self.mu_low = mu_low
        self.mu_high = mu_high
        self.r = random.Random(seed)
        self.rng = np.random.default_rng(seed=seed)
        self.seed = seed

    def _graph_preprocess(self, new_graph: Optional[nx.Graph]) -> nx.Graph:
        if new_graph is None:
            return
        self.label_map = {i: n for i, n in enumerate(new_graph.nodes())}
        return nx.relabel.convert_node_labels_to_integers(new_graph)

    def _graph_postprocess(self) -> nx.Graph:
        g = self._graph.copy()
        for _, _, d in g.edges(data=True):
            d.clear()
        return nx.relabel.relabel_nodes(g, self.label_map)

    def _partition_postprocess(self, partition: ct.Partition) -> ct.Partition:
        relabel = lambda x: [self.label_map[i] for i in x]
        return [relabel(community) for community in partition]

    def tournament(
        self,
        n_parents: int,
        n_tournament: int,
    ) -> ct.Population:
        parents = []
        for _ in range(n_parents):
            candidates = [
                self.population[self.r.randrange(0, len(self.population))]
                for _ in range(n_tournament)
            ]
            parents.append(get_fittest(self._graph, candidates, self.fitness)[0])
        return parents

    def flat_recomb(
        self, parent_a: ct.Partition, parent_b: ct.Partition
    ) -> ct.Partition:
        overlay_clus = overlay_cluster(parent_a, parent_b)
        return self.louvain.partition(
            graph=self._graph, partition=overlay_clus, seed=self.seed
        )[0]

    def get_induced(
        self, overlay_clus: ct.Partition, fitter: ct.Partition
    ) -> Tuple[nx.Graph, ct.Partition]:
        def get_partition(overlay_clus: ct.Partition, node: int) -> int:
            for i, lst in enumerate(overlay_clus):
                try:
                    lst.index(node)
                    return i
                except:
                    pass
            return -1

        clusters = list(range(len(overlay_clus)))
        ind_graph = nx.Graph()
        ind_graph.add_nodes_from(clusters)
        for n1, n2, data in self._graph.edges(data=True):
            w = data.get("weight", 1)
            c1 = get_partition(overlay_clus, n1)
            c2 = get_partition(overlay_clus, n2)
            w_current = ind_graph.get_edge_data(c1, c2, {"weight": 0}).get("weight", 1)
            ind_graph.add_edge(c1, c2, **{"weight": w + w_current})

        same_cluster = {}
        for i in clusters:
            same_cluster[i] = []
            ci = overlay_clus[i]
            j = 0
            while j < i:
                c = overlay_clus[j].copy()
                c.extend(ci)
                same = True
                for partition in fitter:
                    for k in c:
                        if k not in partition:
                            same = False
                            break
                if same:
                    same_cluster[i].append(j)
                j += 1

        ind_partition = []
        already_included = []
        for i in sorted(clusters, reverse=True):
            if len(same_cluster[i]) > 0:
                if i not in already_included:
                    ind_partition.append(same_cluster[i])
            else:
                ind_partition.append([i])

        return ind_graph, ind_partition

    def deinduce(
        self, overlay_clus: ct.Partition, induced_result: ct.Partition
    ) -> ct.Partition:
        partition = []
        for ind_part in induced_result:
            deind_part = []
            for node in ind_part:
                deind_part.extend(overlay_clus[node])
            partition.append(deind_part)
        return partition

    def apply_in_recomb(
        self, parent_a: ct.Partition, parent_b: ct.Partition
    ) -> ct.Partition:
        overlay_clus = overlay_cluster(parent_a, parent_b)
        fitter = get_fittest(self._graph, [parent_a, parent_b], self.fitness)[0]
        induced_graph, induced_partition = self.get_induced(overlay_clus, fitter)
        induced_result = self.louvain.partition(
            graph=induced_graph, partition=induced_partition, seed=self.seed
        )[0]
        return self.deinduce(overlay_clus, induced_result)

    def cluster_recomb(
        self, parent_a: ct.Partition, parent_b: ct.Partition
    ) -> ct.Partition:
        fitter = get_fittest(self._graph, [parent_a, parent_b], self.fitness)[0]
        n = self._graph.number_of_nodes()
        u = self.rng.uniform(self.u_low(n), self.u_high(n))
        new_cluster = self.label_prop.partition(
            graph=self._graph,
            th=self.th,
            max_community_size=u,
        )
        overlay_clus = overlay_cluster(fitter, new_cluster)
        return self.louvain.partition(
            graph=self._graph, partition=overlay_clus, seed=self.seed
        )[0]

    def recombine(self, parent_a: ct.Partition, parent_b: ct.Partition) -> ct.Partition:
        fn = self.r.choice(
            [self.flat_recomb, self.apply_in_recomb, self.cluster_recomb]
        )
        return fn(parent_a, parent_b)

    def new_generation(self) -> ct.Partition:
        offspring = self.recombine(*self.tournament(self.pn, self.k))
        if self.r.random() >= (1 - self.mp):
            offspring = self.mutate(offspring)
        return offspring

    def mutate(self, partition: ct.Partition) -> ct.Partition:
        n = int(self.rng.uniform(self.mu_low(partition), self.mu_high(partition)))
        for _ in range(n):
            new_partition = []
            i = self.r.choice(range(len(partition)))
            for j, part in enumerate(partition):
                if j != i:
                    new_partition.append(part)
                else:
                    new_partition.extend(
                        nx.community.kernighan_lin_bisection(self._graph.subgraph(part))
                    )
            partition = new_partition
        return partition

    def selection(self, offspring: ct.Partition) -> None:
        fit = lambda x: self.fitness(self._graph, x)
        offspring_fit = fit(offspring)
        population_fit = [fit(p) for p in self.population]
        lesser_fit = [
            (self.population[i], i)
            for i, p in enumerate(population_fit)
            if p < offspring_fit
        ]
        if len(lesser_fit) > 0:
            cut_edges = lambda x: sum(
                [
                    nx.algorithms.cuts.cut_size(self._graph, i, j)
                    for i, j in product(x, x)
                ]
            )
            sim = lambda x, y: self._graph.number_of_edges() - abs(cut_edges(x) - cut_edges(y))
            similarity = {i: sim(offspring, n) for n, i in lesser_fit}
            most_similar = max(similarity.items(), key=lambda x: x[1])[0]
            new_population = [
                p for i, p in enumerate(self.population) if i != most_similar
            ]
            new_population.append(offspring)
            self.population = new_population

    def init_population(
        self,
    ):
        n = self._graph.number_of_nodes()
        u = self.rng.uniform(self.u_low(n), self.u_high(n))
        population = []
        nx.set_edge_attributes(self._graph, 1, "weight")

        partitions = []
        for _ in range(self.n):
            partitions = []
            new_partition = self.label_prop.partition(
                graph=self._graph,
                max_rounds=self.r.randrange(self.lambda_low, self.lambda_high + 1),
                th=self.th,
                max_community_size=u,
            )
            if new_partition is not None:
                partitions.append(new_partition)
                partitions.append(
                    self.louvain.partition(self._graph, new_partition, seed=self.seed)[
                        0
                    ]
                )
                population.append(get_fittest(self._graph, partitions, self.fitness)[0])
            else:
                population.append(
                    self.louvain.partition(self._graph, seed=self.seed)[0]
                )

        self.population = population

    def _max_fit(self) -> float:
        return max([self.fitness(self._graph, p) for p in self.population])

    def run(self) -> List[Any]:
        change = True
        self.init_population()
        fitness_records = [self._max_fit()]
        while change:
            offspring = self.new_generation()
            self.selection(offspring)
            fitness_records.append(self._max_fit())
            if len(fitness_records) > self.nc_iters:
                latest = fitness_records[-self.nc_iters :]
                if (
                    abs(min(latest) - max(latest)) <= self.c_th
                    or len(fitness_records) >= self.max_iters
                ):
                    change = False

        last_gen_fittest, last_gen_fitness = get_fittest(
            self._graph, self.population, self.fitness
        )
        fitness_records.append(max(last_gen_fitness))
        return self._partition_postprocess(last_gen_fittest), fitness_records

    def partition(
        self,
        graph: Optional[nx.Graph] = None,
        seed: Optional[int] = None,
    ) -> Tuple[ct.Partition, List[ct.Fitness]]:
        if seed is not None:
            self.r = random.Random(seed)
            self.rng = np.random.default_rng(seed=seed)
        if graph:
            self._graph = self._graph_preprocess(graph)
        return self.run()
