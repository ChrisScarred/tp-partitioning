from collections import defaultdict
from typing import Any, Callable, List, Optional

import networkx as nx

import numpy as np
import src.utils.custom_typing as ct
from sklearn.cluster import SpectralClustering as spectralclustering
from src.partitioners.models import Partitioner
from src.utils.config import FIT, MAX_COMMUNITIES, SEED


class SpectralClustering(Partitioner):
    def __init__(self, graph: Optional[nx.Graph] = None, fit: Callable = FIT) -> None:
        self._graph = graph
        self.fitness = fit

    def spectral_clustering(
        self, adj_matrix: np.ndarray, n_clusters: int, seed: int = SEED
    ) -> ct.Partition:
        community = spectralclustering(
            n_clusters=n_clusters,
            affinity="precomputed",
            assign_labels="kmeans",
            random_state=seed,
        ).fit_predict(adj_matrix)
        community_dict = defaultdict(list)
        [community_dict[value].append(index) for index, value in enumerate(community)]
        return list(community_dict.values())

    def partition(
        self,
        graph: Optional[nx.Graph] = None,
        max_communities: int = MAX_COMMUNITIES,
        seed: int = SEED,
    ) -> List[Any]:
        """Find the best partition of the graph by iterating the Spectral Clustering
        with an increasing number of communities.
        """
        if graph:
            self._graph = graph

        adj_matrix = nx.to_numpy_array(self._graph)

        modularity_list = []
        community_list = []

        number_of_partitions = 0
        i = 0

        while number_of_partitions < min(adj_matrix.shape[0], max_communities):
            partitioning = self.spectral_clustering(
                adj_matrix, n_clusters=i + 2, seed=seed
            )
            community_list.append(partitioning)
            mod = 0
            try:
                mod = nx.community.modularity(self._graph, community_list[i])
            except nx.algorithms.community.quality.NotAPartition:
                pass
            modularity_list.append(mod)
            number_of_partitions = len(community_list[i])
            i += 1

        return community_list[np.argmax(modularity_list)], modularity_list
