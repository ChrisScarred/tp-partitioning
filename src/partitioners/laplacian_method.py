from typing import List, Optional, Tuple

import networkx as nx
import numpy as np
import src.utils.custom_typing as ct
from numpy.linalg import eig
from src.partitioners.models import Partitioner
from src.utils.config import MAX_COMMUNITIES

class LaplacianMethod(Partitioner):
    def __init__(self, graph: Optional[nx.Graph] = None) -> None:
        self._graph = graph

    def get_eigenvectors(self, laplacian_matrix: np.ndarray) -> np.ndarray:
        """Get eigenvectors sorted based on their corresponding eigenvalues, from smallest to largest.
        Ignores imaginary parts."""
        eval, evec = eig(laplacian_matrix)
        eval = np.real(eval)
        evec = np.real(evec)
        return evec[:, np.argsort(eval)]

    def laplacian_method(self, eigenvectors: np.ndarray) -> ct.Partition:
        community_dict = {}
        for i, row in enumerate(eigenvectors):
            sign_combination = tuple([True if value >= 0 else False for value in row])
            if sign_combination in community_dict:
                community_dict[sign_combination].append(i)
            else:
                community_dict[sign_combination] = [i]

        return list(community_dict.values())

    def partition(
        self,
        graph: Optional[nx.Graph] = None,
        max_communities: int = MAX_COMMUNITIES,
        seed: Optional[int] = None,
    ) -> Tuple[ct.Partition, List[ct.Fitness]]:
        """
        Find the best partition of a graph G by iterating the Laplacian method
        with an increasing number of eigenvectors considered.
        """
        graph = graph or self._graph

        laplacian_matrix = nx.normalized_laplacian_matrix(graph).toarray()
        community_list = []
        modularity_list = []
        eigenvectors = self.get_eigenvectors(laplacian_matrix)

        number_of_partitions = 0
        i = 0

        while (
            number_of_partitions < min(laplacian_matrix.shape[0], max_communities)
            and i < 7
        ):

            community_list.append(self.laplacian_method(eigenvectors[:, 1 : (i + 2)]))
            mod = 0
            try:
                mod = nx.community.modularity(graph, community_list[i])
            except nx.algorithms.community.quality.NotAPartition:
                pass
            modularity_list.append(mod)
            number_of_partitions = len(community_list[i])
            i += 1

        return community_list[np.argmax(modularity_list)], modularity_list
