from abc import ABC, abstractmethod
from typing import Callable, List, Tuple

import networkx as nx
import src.utils.custom_typing as ct


class Partitioner(ABC):
    @abstractmethod
    def partition(self): ...


class GeneticAlgorithm(ABC):
    """Base class for genetic algorithms (GA) containing the necessary components of a GA:
    - initiation of the population,
    - creation of the new generation,
    - a selection procedure,
    - a run function which contains the necessary procedures for iterative runs of the GA.
    """

    @abstractmethod
    def new_generation(self):
        """Abstract method for creation of a new generation.

        Implementations may utilise a tournament selection,
        crossover, mutation, or local search (not an exhaustive list).
        """
        ...

    @abstractmethod
    def selection(self):
        """Abstract method for selection of the next generation.

        Implementations may utilise a generational replacement,
        (partial) elitism, or other components, often based on a fitness function.
        """
        ...

    @abstractmethod
    def init_population(self):
        """Abstract method for creation of the initial population.

        Implementations may utilise a random generator or a fixed initial population.
        """
        ...

    @abstractmethod
    def run(self):
        """Abstract method for running the entire algorithm.

        Implementations may utilise parameter retrieval and/or setting techniques and format transformations.
        """
        ...


def get_fittest(
    graph: nx.Graph,
    partitions: List[ct.Partition],
    fitness_func: Callable,
) -> Tuple[ct.Partition, List[ct.Fitness]]:
    modularities = {}
    for i, partition in enumerate(partitions):
        modularities[i] = fitness_func(graph, partition)
    target = max(modularities.items(), key=lambda x: x[1])[0]
    return partitions[target], list(modularities.values())
