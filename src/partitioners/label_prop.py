import math
import random
from functools import reduce
from itertools import product
from typing import Any, Callable, Dict, Iterable, List, Optional, Tuple

import networkx as nx
import src.utils.custom_typing as ct
from src.partitioners.models import Partitioner

from src.utils.config import FIT, LP_MAX_ITERS, SEED


class LabelProp(Partitioner):
    def __init__(
        self,
        graph: Optional[nx.Graph] = None,
        fit: Callable = FIT,
        max_iters: int = LP_MAX_ITERS,
        seed: int = SEED,
    ) -> None:
        self._graph = graph
        self.fitness = fit
        self.max_iters = max_iters
        self.r = random.Random(seed)

    def _cluster_score(self, edges: List[ct.WeightedEdge], cluster: ct.Cluster) -> int:
        return sum(edges.get((i, j), 0) for (i, j) in product(cluster, cluster))

    def _partition_scores(
        self, edges: List[ct.WeightedEdge], partition: ct.Partition
    ) -> List[int]:
        return [self._cluster_score(edges, clus) for clus in partition]

    def _partition_counts(self, partition: ct.Partition) -> List[int]:
        return [len(clus) for clus in partition]

    def _cluster_id_of_node(self, partition: ct.Partition, node: Any) -> int:
        return reduce(
            lambda x, y: (x[0] + y[0],) if node in y[1] else (x[0],),
            enumerate(partition),
        )[0]

    def _target_clusters(
        self, node: Any, partition: ct.Partition, graph: nx.Graph
    ) -> List[ct.Cluster]:
        target_clusters = [
            self._cluster_id_of_node(partition, neighbour)
            for neighbour in graph.neighbors(node)
        ]
        current_cluster = self._cluster_id_of_node(partition, node)
        if current_cluster in target_clusters:
            target_clusters.remove(current_cluster)

        return target_clusters

    def _cluster_stats(
        self,
        partition: ct.Partition,
        target_clusters: List[ct.Cluster],
        edges: List[ct.WeightedEdge],
    ) -> Tuple[List[int], List[int]]:
        p_scores = self._partition_scores(edges, partition)
        p_counts = self._partition_counts(partition)
        target_scores = [p_scores[i] for i in target_clusters]
        target_counts = [p_counts[i] for i in target_clusters]
        return target_scores, target_counts

    def _get_candidates(
        self,
        node: Any,
        partition: ct.Partition,
        graph: nx.Graph,
        max_community_size: Optional[int],
        edges: List[ct.WeightedEdge],
    ) -> List[ct.Cluster]:
        if not isinstance(max_community_size, int):
            max_community_size = math.inf
        target_clusters = self._target_clusters(node, partition, graph)
        target_scores, target_counts = self._cluster_stats(
            partition, target_clusters, edges
        )
        if len(target_clusters) > 0:
            max_score = max(target_scores)
            target_indices = [
                i for i, score in enumerate(target_scores) if score == max_score
            ]
            return list(
                set(
                    [
                        target_clusters[i]
                        for i in target_indices
                        if target_counts[i] < max_community_size
                    ]
                )
            )
        return []

    def coarsen(
        self, graph: nx.Graph, partition: ct.Partition
    ) -> Tuple[nx.Graph, Dict]:
        scheme = self.coarse_partition_to_scheme(partition)
        new_nodes = scheme.keys()
        new_graph = nx.Graph()
        new_graph.add_nodes_from(new_nodes)
        for i, j in product(new_nodes, new_nodes):
            if i != j:
                k = nx.cut_size(graph, scheme[i], scheme[j])
                if k > 0:
                    new_graph.add_edge(i, j, weight=k)
            else:
                sub = nx.subgraph(graph, scheme[i])
                k = sub.number_of_edges()
                new_graph.add_edge(i, j, weight=k)
        return new_graph, scheme

    def coarse_partition_to_scheme(self, partition: ct.Partition) -> Dict:
        return {i: nodes for i, nodes in enumerate(partition)}

    def uncoarsen(self, schemes: List[Dict]) -> ct.Partition:
        if len(schemes) == 1:
            return list(map(lambda x: list(x), schemes[0].values()))
        partition = list(map(lambda x: list(x), schemes[-1].values()))
        for i in range(len(schemes) - 1):
            current_scheme = schemes[-(i + 2)]
            new_partition = []
            for p in partition:
                new_p = []
                for j in p:
                    mapped = current_scheme.get(j)
                    if isinstance(mapped, Iterable):
                        new_p.extend(mapped)
                    elif mapped is not None:
                        new_p.append(mapped)
                if len(p) > 0:
                    new_partition.append(new_p)
            partition = new_partition
        return partition

    def round(
        self,
        graph: nx.Graph,
        max_iters: int,
        th: Optional[float],
        max_community_size: Optional[int],
        init_partition: Optional[ct.Partition] = None,
    ) -> ct.Partition:
        nodes = list(graph.nodes())
        n = len(nodes)
        edges = {(i, j): w for i, j, w in graph.edges.data("weight", default=1)}
        partition = init_partition if init_partition else [[n] for n in graph.nodes()]

        changed = n
        for _ in range(max_iters):
            if isinstance(th, float):
                if changed / n < th:
                    return partition
            changed = 0
            self.r.shuffle(nodes)
            for node in nodes:
                candidates = self._get_candidates(
                    node, partition, graph, max_community_size, edges
                )
                current_cluster = self._cluster_id_of_node(partition, node)

                if len(candidates) > 0:
                    changed += 1
                    i = self.r.choice(candidates)
                    partition[i].append(node)
                    partition[current_cluster].remove(node)
                    if len(partition[current_cluster]) == 0:
                        partition.remove([])

        return partition

    def label_propagate(
        self,
        max_iters: int,
        max_rounds: Optional[int],
        th: Optional[float],
        max_community_size: Optional[int],
        partition: Optional[ct.Partition],
    ) -> ct.Partition:
        if max_rounds is not None:
            if max_rounds <= 0:
                return
        if not partition:
            partition = [{n} for n in self._graph.nodes()]
        schemes = []
        current_graph = self._graph.copy()
        change = True
        i = 0
        while change:
            new_partition = self.round(
                current_graph, max_iters, th, max_community_size
            )
            new_graph, scheme = self.coarsen(current_graph, new_partition)
            if not nx.utils.misc.graphs_equal(new_graph, current_graph):
                schemes.append(scheme)
                partition = self.uncoarsen(schemes)
                current_graph = new_graph
                i += 1
                if isinstance(max_rounds, int):
                    if i > max_rounds:
                        change = False
            else:
                change = False
        return partition

    def partition(
        self,
        graph: Optional[nx.Graph] = None,
        max_rounds: Optional[int] = None,
        max_iters: Optional[int] = None,
        th: Optional[float] = None,
        max_community_size: Optional[int] = None,
        partition: Optional[ct.Partition] = None,
        seed: Optional[int] = None
    ) -> ct.Partition:
        if seed is not None:
            self.r = random.Random(seed)
        if graph:
            self._graph = graph
        if not isinstance(max_iters, int):
            max_iters = self.max_iters
        return self.label_propagate(
            max_iters, max_rounds, th, max_community_size, partition
        )
