import random
from typing import Callable, Dict, List, Optional, Tuple

import networkx as nx
import src.utils.custom_typing as ct
from community import community_louvain
from src.graph_ops.representation import partition_as_dict, partition_from_dict
from src.partitioners.models import get_fittest, Partitioner
from src.utils.config import FIT, SEED
import numpy as np

class Louvain(Partitioner):
    def __init__(
        self, graph: Optional[nx.Graph] = None, fit: Callable = FIT, seed: int = SEED
    ) -> None:
        self._graph = graph
        self.fitness = fit
        self.seed = seed

    def _partitions_from_dendro(self, dendro: List[Dict]) -> List[ct.Partition]:
        return [
            partition_from_dict(
                community_louvain.partition_at_level(dendro, i)
            )
            for i in range(len(dendro))
        ]

    def partition(
        self,
        graph: Optional[nx.Graph] = None,
        partition: Optional[ct.Partition] = None,
        seed: Optional[int] = None,
    ) -> Tuple[ct.Partition, List[ct.Fitness]]:
        if seed is not None:
            self.seed = seed
        graph = graph or self._graph
        return get_fittest(
            graph,
            self._partitions_from_dendro(
                community_louvain.generate_dendrogram(
                    graph,
                    partition_as_dict(partition) if partition else None,
                    random_state=self.seed,
                )
            ),
            self.fitness,
        )
