import itertools

import os
import textwrap
import time
from typing import Dict, Iterable, List, Optional, Tuple

import matplotlib as mpl
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import seaborn as sns
import src.utils.custom_typing as ct
from sklearn.preprocessing import MinMaxScaler
from src.utils.config import (
    AX_MARGIN,
    COLOUR_LIST,
    DIR_FIGS,
    DRAW_LABELS,
    EDGE_ALPHA,
    EDGE_COLOUR,
    FIT_LINESTYLE,
    FIT_MARKER,
    FIT_TITLE,
    FIT_XLABEL,
    FIT_YLABEL,
    FONT_FAMILY,
    NODE_COLOUR,
    RELABEL,
    SAVE_BEESWARM,
    SAVE_GRAPHS,
    SAVE_MOD,
    SEED,
    SHOW_BEESWARM,
    SHOW_GRAPHS,
    SHOW_MOD,
    WEIGHT_ATTR,
)


def validate_communities(graph: nx.Graph, communities: Optional[ct.Partition]) -> None:
    if communities:
        if len(communities) > 0:
            assert graph.number_of_nodes() == len(
                list(itertools.chain.from_iterable(communities))
            ), "Each node must belong to exactly one community."


def draw(
    graph: nx.Graph,
    communities: Optional[ct.Partition] = None,
    relabel: bool = RELABEL,
    node_colour: str = NODE_COLOUR,
    font_family: str = FONT_FAMILY,
    edge_colour: str = EDGE_COLOUR,
    edge_alpha: float = EDGE_ALPHA,
    ax_margins: float = AX_MARGIN,
    seed: int = SEED,
    draw_labels: bool = DRAW_LABELS,
    colours: List[ct.Colour] = COLOUR_LIST,
    show_graphs: bool = SHOW_GRAPHS,
    save_graphs: bool = SAVE_GRAPHS,
    fig_dir: str = DIR_FIGS,
    title: Optional[str] = None,
) -> None:
    validate_communities(graph, communities)
    if relabel:
        graph = relabel_graph(graph)
    layout = get_layout(graph, seed)
    node_size, font_size, edge_width = get_visual_params(graph.number_of_nodes())
    draw_nodes(
        graph,
        layout,
        0 if not isinstance(communities, Iterable) else len(communities),
        communities,
        colours,
        node_size,
        node_colour,
        draw_labels,
        font_size,
        font_family,
    )
    draw_edges(graph, layout, edge_width, edge_alpha, edge_colour)
    show_with_plt(ax_margins, show_graphs, save_graphs, fig_dir, title)


def relabel_graph(graph: nx.Graph) -> nx.Graph:
    labels = dict(zip(sorted(graph.nodes), np.arange(0, graph.number_of_nodes() - 1)))
    return nx.relabel_nodes(graph, labels)


def get_visual_params(n_nodes: int) -> Tuple[int, int, int]:
    node_size = max(min(700, 10**4 * n_nodes ** (-0.7)), 5)
    label_size = max(min(16, 10**2 * n_nodes ** (-0.4)), 5)
    edge_width = max(min(5, 40 * n_nodes ** (-0.6)), 0.1)
    return node_size, label_size, edge_width


def get_layout(graph: nx.Graph, seed: int) -> ct.Layout:
    return nx.spring_layout(graph, seed=seed)


def _get_colour_list(colours: List[ct.Colour], n: int) -> List[ct.Colour]:
    if n <= len(colours):
        return colours[:n]
    else:
        return np.array(mpl.cm.gist_rainbow(np.linspace(0, 1, n)))


def draw_nodes(
    graph: nx.Graph,
    pos: ct.Layout,
    n_comm: int,
    communities: Optional[ct.Partition],
    colours: List[ct.Colour],
    node_size: int,
    node_colour: str,
    draw_labels: bool,
    font_size: int,
    font_family: str,
) -> None:
    if n_comm > 0:
        for colour, community in zip(
            _get_colour_list(colours, n_comm),
            communities,
        ):
            nx.draw_networkx_nodes(
                graph,
                pos,
                nodelist=list(community),
                node_size=node_size,
                node_color=colour,
            )
    else:
        nx.draw_networkx_nodes(graph, pos, node_size=node_size, node_color=node_colour)

    if draw_labels:
        nx.draw_networkx_labels(
            graph, pos, font_size=font_size, font_family=font_family
        )


def draw_edges(
    graph: nx.Graph,
    pos: ct.Layout,
    edge_width: int,
    edge_alpha: float,
    edge_colour: str,
    weight_attribute: str = WEIGHT_ATTR,
) -> None:
    """Partially sourced from https://qxf2.com/blog/drawing-weighted-graphs-with-networkx/."""
    edge_labels = nx.get_edge_attributes(graph, weight_attribute)
    if edge_labels:
        all_weights = list(edge_labels.values())
        scaler = MinMaxScaler(feature_range=(edge_width - 1, edge_width + 1))
        model = scaler.fit(np.array(all_weights).reshape(-1, 1))
        unique_weights = list(set(all_weights))

        for weight in unique_weights:
            weighted_edges = [
                (node1, node2)
                for (node1, node2, edge_attr) in graph.edges(data=True)
                if edge_attr["weight"] == weight
            ]
            weighted_edge_width = np.ravel(
                model.transform(np.array(weight).reshape(1, -1))
            )
            nx.draw_networkx_edges(
                graph,
                pos,
                width=weighted_edge_width,
                alpha=edge_alpha,
                edge_color=edge_colour,
                edgelist=weighted_edges,
            )
    else:
        nx.draw_networkx_edges(
            graph, pos, width=edge_width, alpha=edge_alpha, edge_color=edge_colour
        )


def show_with_plt(
    ax_margins: float,
    show_graphs: bool = SHOW_MOD,
    save_graphs: bool = SAVE_MOD,
    fig_dir: str = DIR_FIGS,
    title: Optional[str] = None,
) -> None:
    ax = plt.gca()
    ax.margins(ax_margins)
    plt.axis("off")
    plt.tight_layout()
    if save_graphs:
        title = title or ""
        path = os.path.abspath(fig_dir)
        plt.savefig(os.path.join(path, f"{title.replace(' ', '_')}{time.time()}.svg"))
    if show_graphs:
        plt.show()
    else:
        plt.close()


def beeswarm_plot(
    data: Dict[str, List[float]],
    title: str = "",
    fig_dir: str = DIR_FIGS,
    xlabel: str = "Algorithm",
    ylabel: str = FIT_YLABEL,
    seed: int = SEED,
    show_beeswarm: bool = SHOW_BEESWARM,
    save_beeswarm: bool = SAVE_BEESWARM,
):
    np.random.seed(seed)
    _, ax = plt.subplots()
    sns.stripplot(data, ax=ax)
    if len(title) > 0:
        ax.set_title(textwrap.fill(title))
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    if save_beeswarm:
        path = os.path.abspath(fig_dir)
        plt.savefig(os.path.join(path, f"{title.replace(' ', '_')}{time.time()}.svg"))
    if show_beeswarm:
        plt.show()
    else:
        plt.close()


def plot_fitness(
    fitness_values: List[float],
    xlabel: str = FIT_XLABEL,
    ylabel: str = FIT_YLABEL,
    title: str = FIT_TITLE,
    ls: str = FIT_LINESTYLE,
    marker: str = FIT_MARKER,
    show_graphs: bool = SHOW_MOD,
    save_graphs: bool = SAVE_MOD,
    fig_dir: str = DIR_FIGS,
) -> None:
    plt.plot(
        list(range(len(fitness_values))),
        fitness_values,
        linestyle=ls,
        marker=marker,
    )
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(textwrap.fill(title))
    if save_graphs:
        path = os.path.abspath(fig_dir)
        plt.savefig(os.path.join(path, f"{title.replace(' ', '_')}{time.time()}.svg"))
    if show_graphs:
        plt.show()
    else:
        plt.close()
