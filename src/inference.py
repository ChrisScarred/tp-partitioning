import time
import tracemalloc
from typing import List, Optional, Tuple

import networkx as nx
import pandas as pd
import src.utils.custom_typing as ct
from src.graph_ops.representation import get_graphs, process_results, aggregate_data

from src.partitioners.laplacian_method import LaplacianMethod
from src.partitioners.louvain import Louvain
from src.partitioners.memevo import MemEvo
from src.partitioners.spectral_clustering import SpectralClustering
from src.utils.config import REPEAT, SHOW_AGG, SAVE_AGG

def infer(
    adj_dir: Optional[str],
    adj_path: Optional[str],
    save_parts: bool,
    part_dir: str,
    print_stats: bool,
    save_stats: bool,
    stats_dir: bool,
    show_nets: bool,
    show_mod: bool,
    save_graphs: bool,
    save_mod: bool,
    dir_figs: bool,
    algorithm_comparison: bool,
    repeat: int = REPEAT,
    show_agg: bool = SHOW_AGG,
    save_agg: bool = SAVE_AGG,
) -> None:
    if not algorithm_comparison:
        repeat = 1
    final_data = pd.DataFrame()
    for i in range(repeat):
        graphs = get_graphs(adj_dir, adj_path, algorithm_comparison)
        algorithms = get_algorithms(algorithm_comparison)
        results = perform_partitioning_of_graphs(graphs, algorithms, i)
        data = process_results(
            results,
            save_parts,
            part_dir,
            print_stats,
            save_stats,
            stats_dir,
            show_nets,
            algorithm_comparison,
            show_mod,
            save_graphs,
            save_mod,
            dir_figs,
        )
        if algorithm_comparison:
            data["run"] = i
            final_data = pd.concat([final_data, data], ignore_index=True)
        else:
            final_data = data
    if algorithm_comparison:
        aggregate_data(final_data, repeat, show_agg, save_agg, stats_dir)


def get_algorithms(algorithm_comparison: bool) -> List[ct.AlgorithmInfo]:
    algorithms = [("MemEvo", MemEvo())]
    if algorithm_comparison:
        algorithms.extend(
            [
                ("Louvain", Louvain()),
                ("Spectral Clustering", SpectralClustering()),
                ("Laplacian Method", LaplacianMethod()),
            ]
        )
    return algorithms


def perform_partitioning_of_graphs(
    graphs: ct.GraphInfo, algorithms: ct.AlgorithmInfo, seed: Optional[int] = None
) -> pd.DataFrame:
    results = []
    for algorithm in algorithms:
        results.extend(
            [list(get_partitions(graph_info, algorithm, seed)) for graph_info in graphs]
        )

    return pd.DataFrame(
        data=results,
        columns=[
            "network_name",
            "algorithm_name",
            "network",
            "ground_truth",
            "partition",
            "fitness",
            "memory",
            "peak",
            "exec_time",
        ],
    )


def get_partitions(
    graph_info: ct.GraphInfo,
    algorithm_info: ct.AlgorithmInfo,
    seed: Optional[int] = None
) -> Tuple[
    ct.GraphName,
    ct.AlgName,
    nx.Graph,
    ct.GroundTruth,
    ct.Partition,
    List[ct.Fitness],
    ct.Memory,
    ct.Memory,
    ct.Time,
]:
    alg_name, algorithm = algorithm_info
    graph_name, (graph, ground_truth) = graph_info
    tracemalloc.reset_peak()
    tracemalloc.clear_traces()
    tracemalloc.start()
    start = time.time()

    partition, fitness = algorithm.partition(graph=graph.copy(), seed=seed)

    end = time.time()
    memory, peak = tracemalloc.get_traced_memory()
    tracemalloc.stop()

    return (
        graph_name,
        alg_name,
        graph,
        ground_truth,
        partition,
        fitness,
        memory,
        peak,
        end - start,
    )
